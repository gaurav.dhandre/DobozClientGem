require "faraday"
require "openssl"
require "json"
require "securerandom"

require "doboz_client"
require "stripe"

require "doboz_stripe/version"

require "doboz_stripe/translator"
require "doboz_stripe/split_tender_validator"

require "doboz_stripe/wrappers/doboz_charge"
require "doboz_stripe/wrappers/doboz_fund"
require "doboz_stripe/wrappers/doboz_value"
require "doboz_stripe/wrappers/refund"

require "doboz_stripe/stripe_doboz_split_tender_charge"

module Doboz
  class << self
    attr_accessor :api_base, :api_key
  end
  @api_base = 'http://localhost:3000/api/v1/woocommerce/'
end
