module Doboz
  class DobozValue < Doboz::DobozObject
    attr_accessor :giftcard, :currency, :cardType, :asAtDate, :cardId

    def self.retrieve_code_details (code)
      Doboz::Validator.validate_code! (code)
      response = Doboz::Code.get_details(code)
      self.new(response)
    end

    def self.retrieve_card_details (card_id)
      Doboz::Validator.validate_card_id!(card_id)
      response = Doboz::Card.get_details(card_id)
      self.new(response)
    end

    def self.retrieve_contact_account_details (contact_id, currency)
      Doboz::Validator.validate_contact_id!(contact_id)
      Doboz::Validator.validate_currency!(currency)
      response = Doboz::Account.get_account_details({contact_id: contact_id, currency: currency})
      self.new(response)
    end

    def self.retrieve_by_shopper_id (shopper_id, currency)
      Doboz::Validator.validate_shopper_id!(shopper_id)
      Doboz::Validator.validate_currency!(currency)
      response = Doboz::Account.get_account_balance_details({shopper_id: shopper_id, currency: currency})
      self.new(response)
    end


    def maximum_value
      maximum_value = 0
      if self.giftcard['active'] != false
        maximum_value = self.giftcard['remaining_value']
      end
      maximum_value
    end

  end
end