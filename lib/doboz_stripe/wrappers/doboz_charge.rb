module Doboz
  class DobozCharge < Doboz::DobozObject
    attr_accessor :transactionId, :value, :userSuppliedId, :dateCreated, :transactionType, :transactionAccessMethod, :giftbitUserId, :cardId, :currency, :codeLastFour, :metadata

    def self.create (charge_params)
      Doboz::Validator.validate_charge_object! (charge_params)
      charge_params_to_send_to_doboz = Doboz::Translator.charge_params_stripe_to_doboz(charge_params)
      charge_method = charge_params_to_send_to_doboz[:code] ? 'code' : 'cardId'
      response = (charge_method == 'code') ?
          Doboz::Code.charge(charge_params_to_send_to_doboz) :
          Doboz::Card.charge(charge_params_to_send_to_doboz)
      self.new(response)
    end

    def self.simulate (charge_params)
      Doboz::Validator.validate_charge_object! (charge_params)
      charge_params_to_send_to_doboz = Doboz::Translator.charge_params_stripe_to_doboz(charge_params)

      charge_method = charge_params_to_send_to_doboz[:code] ? 'code' : 'cardId'

      response = (charge_method == 'code') ?
          Doboz::Code.simulate_charge(charge_params_to_send_to_doboz) :
          Doboz::Card.simulate_charge(charge_params_to_send_to_doboz)

      self.new(response)
    end

    def cancel! (new_request_body=nil)
      handle_pending(self, 'void', new_request_body)
    end

    def capture! (new_request_body=nil)
      new_request_body = new_request_body
      handle_pending(self, 'redeem', new_request_body)
    end

    private

    def handle_pending (original_transaction_response, void_or_capture, new_request_body=nil)
      hash_of_original_transaction_response = original_transaction_response.clone
      hash_of_original_transaction_response = Doboz::Translator.charge_instance_to_hash!(hash_of_original_transaction_response)
      hash_of_original_transaction_response = hash_of_original_transaction_response["transaction"]
      
      Doboz::Validator.validate_transaction_response!(hash_of_original_transaction_response)

      transaction_id = Doboz::Validator.get_transaction_id(hash_of_original_transaction_response)
      body = new_request_body || {}
      body[:userSuppliedId] ||= Doboz::Validator.get_or_create_user_supplied_id_with_action_suffix(body, transaction_id, void_or_capture)
      response = Doboz::Transaction.handle_transaction(hash_of_original_transaction_response, void_or_capture, body)
      Doboz::DobozCharge.new(response['transaction'])
    end

  end
end
