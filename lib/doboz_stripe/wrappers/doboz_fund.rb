module Doboz
  class DobozFund < Doboz::DobozObject
    attr_accessor :transactionId, :value, :userSuppliedId, :dateCreated, :transactionType, :transactionAccessMethod, :giftbitUserId, :cardId, :currency, :codeLastFour

    def self.create(fund_object)
      Doboz::Validator.validate_fund_object!(fund_object)

      fund_object_to_send_to_doboz = Doboz::Translator.fund_params_stripe_to_lightrail(fund_object)

      response = Doboz::Card.fund(fund_object_to_send_to_lightrail)

      self.new(response)
    end
  end
end
