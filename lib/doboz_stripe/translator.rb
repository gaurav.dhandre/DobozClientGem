module Doboz
  class Translator

    def self.charge_params_stripe_to_doboz(stripe_style_params)
      self.stripe_params_to_doboz!(stripe_style_params, true)
    end

    def self.charge_instance_to_hash!(charge_instance)
      if charge_instance.is_a? Doboz::DobozCharge
        charge_hash = {}
        charge_instance.instance_variables.each {|instance_variable| charge_hash[instance_variable.to_s.delete('@')] = charge_instance.instance_variable_get(instance_variable)}
        charge_hash
      else
        raise Doboz::DobozArgumentError.new("Translator.charge_instance_to_hash! received #{charge_instance.inspect}")
      end
    end

    def self.fund_params_stripe_to_doboz(stripe_style_params)
      self.stripe_params_to_doboz!(stripe_style_params, false)
    end

    def self.construct_doboz_charge_params_from_split_tender(split_tender_charge_params, lr_share)
      doboz_params = split_tender_charge_params.clone
      doboz_params[:pending] ||= doboz_params[:capture] === nil ? false : !doboz_params.delete(:capture)
      doboz_params[:value] = -lr_share
      doboz_params.delete(:amount)

      doboz_params[:contact_id] ||= Doboz::Validator.get_contact_id(doboz_params)
      doboz_params[:shopper_id] ||= Doboz::Validator.get_shopper_id(doboz_params)
      if (doboz_params[:contact_id] || doboz_params[:shopper_id])
        doboz_params = Doboz::Account.replace_contact_id_or_shopper_id_with_card_id(doboz_params)
      end

      doboz_params[:code] ||= Doboz::Validator.get_code(doboz_params)
      doboz_params[:cardId] ||= Doboz::Validator.get_card_id(doboz_params)
      doboz_params[:userSuppliedId] ||= Doboz::Validator.get_or_create_user_supplied_id(doboz_params)

      doboz_params

    end

    def self.construct_doboz_pending_charge_params_from_split_tender(split_tender_charge_params, lr_share)
      doboz_params = self.construct_doboz_charge_params_from_split_tender(split_tender_charge_params, lr_share)
      doboz_params[:pending] = true
      doboz_params
    end

    def self.charge_params_split_tender_to_stripe(split_tender_charge_params, stripe_share)
      stripe_params = split_tender_charge_params.clone
      stripe_params[:amount] = stripe_share

      Doboz::Constants::DOBOZ_PAYMENT_METHODS.each {|charge_param_key| stripe_params.delete(charge_param_key)}
      Doboz::Constants::DOBOZ_USER_SUPPLIED_ID_KEYS.each {|supplied_id_key| stripe_params.delete(supplied_id_key)}
      Doboz::Constants::DOBOZ_USER_CONTACT_KEYS.each {|contact_key| stripe_params.delete(contact_key)}

      stripe_params

    end

    def self.construct_doboz_metadata_for_split_tender_charge(stripe_transaction)
      {
          metadata: {
              splitTenderChargeDetails: {
                  stripeTransactionId: stripe_transaction.id
              }
          }, 

          email: 'aj@gmail.com'

      }
    end

    private

    def self.stripe_params_to_doboz!(transaction_params, convert_amount_to_negative_value)
      lr_transaction_params = transaction_params.clone
      lr_transaction_params[:pending] ||= lr_transaction_params[:capture] === nil ? false : !lr_transaction_params.delete(:capture)
      lr_transaction_params[:userSuppliedId] ||= Doboz::Validator.get_or_create_user_supplied_id(lr_transaction_params)
      lr_transaction_params[:value] ||= convert_amount_to_negative_value ? -(lr_transaction_params[:amount].abs) : lr_transaction_params[:amount].abs

      if (Doboz::Validator.has_valid_contact_id?(lr_transaction_params) || Doboz::Validator.has_valid_shopper_id?(lr_transaction_params))
        lr_transaction_params = Doboz::Account.replace_contact_id_or_shopper_id_with_card_id(lr_transaction_params)
      end
      lr_transaction_params
    end

  end
end